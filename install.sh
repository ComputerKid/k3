#!/bin/sh
#
#Install KDE plasma and i3 window manager to let them work together.

echo 'Begin Installation' > log.txt

echo 'Installing Packages' >> log.txt

echo 'Installing Packages'

./depends.sh

echo 'Applying Windows Manager settings' >> log.txt

echo 'Applying Windows Manager settings'

sudo mkdir ~/.config/plasma-workspace/
sudo mkdir ~/.config/plasma-workspace/env/
sudo touch ~/.config/plasma-workspace/env/wm.sh

sudo echo '# Disable KWin and use i3gaps as WM' > ~/.config/plasma-workspace/env/wm.sh

sudo echo 'export KDEWM=/usr/bin/i3' >> ~/.config/plasma-workspace/env/wm.sh

echo 'Install Backgrounds' 

echo 'Install Backgrounds' >> log.txt

#need to change this name to TBD
sudo mkdir /usr/share/wallpapers/k3

sudo mv ./light-wallpaper.png /usr/share/wallpapers/k3/

echo 'Apply extra KDE specific setting to the i3 config' >> log.txt

echo 'Apply extra KDE specific setting to the i3 config'

sudo mkdir ~/.config/i3/
sudo touch ~/.config/i3/config
sudo echo 'exec --no-startup-id feh --bg-fill /usr/share/wallpapers/k3/k3-wallpaper.png' >> ~/.config/i3/config
sudo echo 'for_window [class="plasmashell"] floating enable' >> ~/.config/i3/config
sudo echo 'for_window [class="Plasma"] floating enable, border none' >> ~/.config/i3/config
sudo echo 'for_window [title="plasma-desktop"] floating enable, border none' >> ~/.config/i3/config
sudo echo 'for_window [title="win7"] floating enable, border none' >> ~/.config/i3/config
sudo echo 'for_window [class="krunner"] floating enable, border none' >> ~/.config/i3/config
sudo echo 'for_window [class="Kmix"] floating enable, border none' >> ~/.config/i3/config
sudo echo 'for_window [class="Klipper"] floating enable, border none' >> ~/.config/i3/config
sudo echo 'for_window [class="Plasmoidviewer"] floating enable, border none' >> ~/.config/i3/config
sudo echo 'no_focus [class="plasmashell" window_type="notification"]' >> ~/.config/i3/config
sudo echo 'for_window [class="plasma.emojier"] floating enable' >> ~/.config/i3/config
sudo echo 'exec --no-startup-id wmctrl -c Plasma' >> ~/.config/i3/config
sudo echo 'for_window [title="Desktop — Plasma"] kill; floating enable;' >> ~/.config/i3/config
sudo echo 'for_window [title="Bureau — Plasma"] kill, floating enable, border none' >> ~/.config/i3/config
sudo echo 'for_window [class="yakuake"] floating enable' >> ~/.config/i3/config

echo 'Ask to install kde-applications' >> log.txt

echo 'Ask to install kde-applications'

./applications.sh
